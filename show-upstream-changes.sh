#!/bin/sh

function help {
	echo -e "$0"
	echo -e "\t\t-h\t\thelp message"
	echo -e "\t\t-n\t\tno pager"
	echo -e "\t\t-c\t\tforce color"
}

# list of arguments expected in the input
optstring=":hnc"

while getopts ${optstring} arg; do
  case ${arg} in
    h)
      help
      ;;
    n)
      NOPAGER=1
      ;;
    c)
      FORCECOLOR=1
      ;;
    :)
      echo "$0: Must supply an argument to -$OPTARG." >&2
      exit 1
      ;;
    ?)
      echo "Invalid option: -${OPTARG}."
      exit 2
      ;;
  esac
done

[ "x$FORCECOLOR" == "x" ] && [ "$NOPAGER" == "1" ] || COLOR="--color=always" COLORHINT="\nIf this shows incorrectly, try opening with \`less -R\` (or use the non-colored version).\n"


OUT="REMEMBER TO FETCH UPSTREAM CHANGES FIRST!\n\
WE CANNOT LIST WHAT WE DON'T KNOW ABOUT!\n\
${COLORHINT}\
\n#########################################\n\n\n\
\
$(git submodule foreach ' \
	# check whether there are any commits at all \
	[ $(git rev-list --count $sha1..origin/master) -gt 0 ] \
\
	&& \
	# print the upstream repo url \
	echo -e "\n################################################\n$(git remote get-url origin)\n################################################\n" \
	&& \
	# show all new commits, including the last that we have (where our HEAD points) \
	git --no-pager log '$COLOR' --decorate=short $sha1~..origin/master --graph \
\
	|| \
	# no commits? cool! print that! \
	echo "no new commits!" \
\
	; \
	# empty line, and another new line for good measure \
	echo -e "\n" \
')"

[ "$NOPAGER" == "1" ] && echo -e "$OUT" || echo -e "$OUT" | less -R
