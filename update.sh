#!/bin/sh

target="${1:-master}"

echo "reset target is $target"

GIT_TERMINAL_PROMPT=0

# update meta repo
git fetch origin "$target" --force
# use the latest commit on master, discard any local changes
git reset --hard origin/"$target" || git reset --hard "$target" || git checkout --force "$target"
# sync repo urls
git submodule sync --recursive
# update commits, discard any local changes
git submodule update --init --recursive --force
# remove unversioned files
git clean -dff
